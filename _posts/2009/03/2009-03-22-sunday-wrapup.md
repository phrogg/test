---
id: 11
title: Sunday Wrapup
date: 2009-03-22T13:39:41+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=11
permalink: /2009/03/sunday-wrapup/
categories:
  - OpenMisc
tags:
  - OpenMinds
  - OpenSociety
---
I will try every sunday to wrap up the topics that kept me busy over the last week. Sort of like a braindump to make sure I capture those loose ends that otherwise get lost.

  * [IBM could buy SUN](http://blogs.zdnet.com/Murphy/?p=1555): Now this caught me by surprise. Watching SUN over the last few months was quite interesting. It was all about Open Source etc. However, I still believe that SUN has for more to offer in the hardware area. Yes, [SPARC](http://en.wikipedia.org/wiki/SPARC) is effectively dying, following [PA-RISC](http://en.wikipedia.org/wiki/PA-RISC). The [x86](http://en.wikipedia.org/wiki/X86) architecture looks like the Big Winner, however I do have my problems with it. But back on topic &#8211; what does this possible takeover mean? Well, IMHO it is dinosaurs uniting. Both SUN and IBM have been fighting over the same accounts for almost twenty years. IBM wants to stoöp this and make the Big Deals a bit cozier. However, seeing how IBM has a lot of inside competition (Power v zSeries v xSeries) I am not convinced. OTOH SUN has decent offerings in the storage area. They are working on some very interesting stuff. Maybe that is the real driver? Time will tell.
  * [TomTom v MSFT:](http://www.heise.de/english/newsticker/news/134937) The underdog strikes back. Remember how TomTom informed the rest of the world that [they spend more money on Patent Stuff as on actual R&D](http://www.youtube.com/watch?v=bSOIK-tlN8g)? Maybe it helps them now.  However, MSFT has again proven to be not that friendly towards Open Source.

That&#8217;s enough for a starter. Expect more to come as I slowly adapt to being a blogger 🙂
